# Flow (moneta2012.pdf)

## Measuring methods
A. Flow Questionnaire (FQ): 
* Divided into two sections
    * Section 1: presents three quotes that
vividly describe the flow experience.
    * Section 2: requires just a yes/no answers.
    * Section 3: asks them to
freely list their flow-conducive activities.
    * Section 4: Select the best flow-conducive activity.
